<?php
function getSelectExplorerAll($db){
    $res = sql($db, "SELECT * FROM `explorer` ", [], 'rows');
    return $res;
}
function getSelectExplorer($db, $id){
    $res = sql($db, "SELECT * FROM `explorer` WHERE `id` =".$id , [], 'rows');
    return $res;
}
function getSelectExplorerId($db, $siteLink)
{
   $res = sql($db, 'SELECT `id` 
            FROM `explorer` 
            WHERE `query`= "' . $siteLink . '"
            AND `status`= 0', [], 'rows');
    return $res;
}
function getSelect($db, $tableName, $query )
{
    $res = sql($db, "SELECT {$query['id']} ,{$query['query']} FROM `$tableName` WHERE {$query['status']} = '0' LIMIT 1", [], 'rows');
    return $res;
}
function getSelectVacancy($db, $id){
    $res = sql($db, "SELECT * FROM `vacancy` WHERE `explorer_id`=$id ORDER BY `create_date` DESC ", [], 'rows');
    return $res;
}
function getSelectVacancyStatus($db){
    $res = sql($db, "SELECT * FROM `vacancy` WHERE `vacancy_status` = '0'", [], 'rows');
    return $res;
}
function insertExplorer($db, $siteLink, $date, $title){
    $insertLinks = $db->prepare("INSERT INTO `explorer` (`query`, `status`, `create_date`, `title`) VALUES (?, ?, ?, ?)");
    $insertLinks->execute(array($siteLink, 0, $date, $title));
}
function insertVacancy($db, $id, $vacancy){
    $insertVacancy = $db->prepare("INSERT INTO `vacancy`(`explorer_id`, `vacancy_url`, `create_date`, `company_name`, `location`,`vacancy_title`,`vacancy_status`) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $insertVacancy->execute(array($id, $vacancy['vacancy_url'], $vacancy['create_date'], $vacancy['company_name'], $vacancy['location'], $vacancy['vacancy_title'], 0));
}
function getCreatDate($db, $table, $col, $id)
{
    $res = sql($db, "SELECT `create_date` FROM `$table` WHERE `$col` =".$id , [], 'rows');
    return $res;
}

function delVac($db, $id){
    $link = sql($db, "DELETE  FROM `vacancy` WHERE `id` =" . $id,
        []
    );
    return $link;
}
function getLink($db, $idRout){
    $link = sql($db, "SELECT `vacancy_url` FROM `vacancy` WHERE `id` = $idRout", [], 'rows');
    return $link;
}
function getSelectAll($db, $tableName, $data, $id){
    $res = sql($db, "SELECT * FROM `$tableName` WHERE `$data`=".$id, [], 'rows');
    return $res;
}
function insertVacancyDescript($db, $id, $strDiscription, $date){
    $insertVacancyDescript = $db->prepare("INSERT INTO vacancy_description(`vacancy_id`, `description`,`status`, `create_date`) VALUES (?, ?, ?, ?)");
    $insertVacancyDescript->execute(array($id, $strDiscription, 0, $date));
}
function insertWord($db, $id,$word, $qty, $date){
    $insertWords = $db->prepare("INSERT INTO `words` (`vacancy_description_id`,`word`, `qty`, `type`, `create_date`) VALUES (?, ?, ?, ?, ?)");
    $insertWords->execute(array($id, $word, $qty, 0, $date));
}
function saveStatus($db, $tableName, $query, $columnStatus)
{
    $resrUpdate = sql($db,
        "UPDATE `$tableName` set 
        `$columnStatus` = 2  
        WHERE `id` =  $query"
    );
    return $resrUpdate;
}
function saveType($db, $id, $word){
    $resType = sql($db,
        'UPDATE `words` set 
        `type` = 1  
        WHERE `word` ="'. $word .'"
        AND `vacancy_description_id` = '.$id
    );
    return $resType;
}
function updateType($db){
    $res= sql($db,
        'UPDATE `words` set 
        `type` = 0'

    );
    return $res;
}
function changeStatus($db)
{
    $resChange = sql($db,
        "UPDATE `explorer` set 
        `status` = 0 
        WHERE `status` =  2"
    );
    return $resChange;
}
function updateStatusExplorer($db, $id){
    $res=sql($db,
        "UPDATE `explorer` set
             `status` = 2
              WHERE `id` =" . $id
    );
    return $res;
}