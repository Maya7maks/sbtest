<?php
function parseVacancyLink($siteLinks){

        $htmlLinks = file_get_html($siteLinks);

    foreach ($htmlLinks->find('nav a[title]') as $element) {

        $lastPage = $element->plaintext;

        $listPages = [];

        for ($i = 1; $i <= $lastPage; $i++) {
            $listPages[] = $siteLinks . '?page=' . $i;
        }

    }
if(isset($listPages)&&!empty($listPages)) {

    $listVacancies = [];
    $key = 0;
    $k = 0;
    $n = 0;
    foreach ($listPages as $htmlPage) {

        foreach (file_get_html($htmlPage)->find('h2.add-bottom-sm a') as $element) {

            $titleDate = $element->title;

            $data = explode(' ', $titleDate);

            $date = array_slice($data, -3, 3);
            switch ($date[1]) {
                case 'января':
                    $date[1] = 1;
                    break;
                case 'февраля':
                    $date[1] = 2;
                    break;
                case 'марта':
                    $date[1] = 3;
                    break;
                case 'апреля':
                    $date[1] = 4;
                    break;
                case 'мая':
                    $date[1] = 5;
                    break;
                case 'июня':
                    $date[1] = 6;
                    break;
                case 'июля':
                    $date[1] = 7;
                    break;
                case 'августа':
                    $date[1] = 8;
                    break;
                case 'сентября':
                    $date[1] = 9;
                    break;
                case 'октября':
                    $date[1] = 10;
                    break;
                case 'ноября':
                    $date[1] = 11;
                    break;
                case 'декабря':
                    $date[1] = 12;
                    break;

            }
            $date = date('Y-m-d', mktime(0, 0, 0, $date[1], (int)$date[0], (int)$date[2]));
            $listVacancies[$key] = [
                'vacancy_url' => 'https://www.work.ua' . str_replace('/ua', '', $element->href),

                'create_date' => $date,
                'company_name' => '',
                'location' => '',
                'vacancy_title' => $element->plaintext];
            $key++;
        }
        foreach (file_get_html($htmlPage)->find('.job-link span b') as $element) {

            $listVacancies[$k]['company_name'] = $element->plaintext;
            $k++;

        }

        foreach (file_get_html($htmlPage)->find('.job-link span') as $element) {
            if ($element->outertext == '<span>· </span>') {
                $location = $element->next_sibling(1)->plaintext;
                $listVacancies[$n]['location'] = substr($location, 0, -16);
                $n++;
            }
        }
    }
    return $listVacancies;
}else{
        return false;
}
}

function getDescript($vacancyDescriptLink)
{
    $site = $vacancyDescriptLink['vacancy_url'];
    if (isset($site)) {
        $html = file_get_html($site);
        $vacDiscript = [];
        foreach (($html->find('div.wordwrap p, div.wordwrap li')) as $elem) {
            if ($elem->find('*[class]')) {
                continue;
            } else {
                if (empty($elem->children())) {
                    $vacDiscript[] = strip_tags($elem->outertext . PHP_EOL);
                } else {
                    foreach ($elem->children() as $child) {
                        if ($child->find('*[class]')) {
                            continue;
                        } else {
                            $vacDiscript[] = strip_tags($child->outertext . PHP_EOL);
                        }
                    }
                }
            }
        }

        $strDiscription = implode('', $vacDiscript);

    }
    return $strDiscription;
}