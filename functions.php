<?php
function view($viewname, $data = []){
    include "views/main.view.php";

    if( file_exists( "views/$viewname.view.php" ) ) {
        include "views/$viewname.view.php";
    }
}
function compareNew($listVacancies, $listVacancExist){

    $newVacancies = [];
    foreach ($listVacancies as $key => $item) {
        $temp = false;
        foreach ($listVacancExist as $value) {
            if($item['vacancy_url']==$value['vacancy_url']&&$item['create_date']==$value['create_date']){
                $temp=true;

            }
        }
        if($temp==false){

            $newVacancies[$key] = [
                'vacancy_url' => $item['vacancy_url'],

                'create_date' => $item['create_date'],
                'company_name'=>$item['company_name'],
                'location' => $item['location'],
                'vacancy_title' => $item['vacancy_title']];

        }
    }
    return $newVacancies;
}

function compareDel($listVacancExist , $listVacancies){
    $delVacancies = [];
    foreach ($listVacancExist as $key1 => $item1) {
        $temp2=false;
        foreach ($listVacancies as $value1) {
            if($item1['vacancy_url']==$value1['vacancy_url']&&$item1['create_date']==$value1['create_date']){
                $temp2=true;

            }
        }
        if($temp2==false){
            echo 2;
            $delVacancies[$key1]=['id'=>$item1['id']];

        }
    }
    return $delVacancies;
}

function pagination( $pagesCount, $section, $page ) {

    for( $p=0; $p < $pagesCount; $p++) {
        $curPage = $page;
        if (($p < $curPage + 3 && $p > $curPage - 3)
            || ($p == 0)
            || ($p == $pagesCount - 1)
        ) {
            echo '<a href="'.$section.'?page='.$p.'">';
            echo ($curPage == $p) ? '<strong>' : '';
            echo $p + 1;
            echo ($curPage == $p) ? '</strong>' : '';
            echo '</a> |';
        }
    }
}


function cleanString($stringDescript){
    $stringDescript = trim($stringDescript);

    $stringDescript = preg_replace("~(\\\|\*|\.|\;|\:|\"|\&|\/|\<|\>|\#|\@|\(|\)|\?|'\?|\!|\,|\[|\?|\]|\(|\\\$|\))~", " ", $stringDescript);


    $stringDescript = htmlentities($stringDescript);
    $stringDescript= str_replace("&nbsp;",' ',$stringDescript);

    $stringDescript = str_replace("\r\n", ' ', $stringDescript);


    $stringDescript = preg_replace("/ +/", " ", $stringDescript);

    $stringDescriptLow = mb_strtolower($stringDescript);
    return $stringDescriptLow;
}


function getWords($stringDescript){
    $stringDescriptLow = cleanString($stringDescript);

    $arrWords = explode(" ", $stringDescriptLow);

    $descriptWord = [];
    $maxWordLength = 2;
    foreach ($arrWords as $word) {
        if (mb_strlen($word) >= $maxWordLength) {
            $descriptWord[] = trim($word);
        }
    }
    $result = array_unique($descriptWord);

    $descriptWordUniq = [];
    $n = 0;
    foreach ($result as $key => $item) {
        $descriptWordUniq[$item] = $n;
    }

    foreach ($descriptWordUniq as $word => $number) {
        foreach ($descriptWord as $elem) {
            if ($word == $elem) {
                $descriptWordUniq[$word] = ++$number;

            }
        }
    }
    return $descriptWordUniq;
}

