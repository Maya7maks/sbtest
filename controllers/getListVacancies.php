<?php
if( $action == 'listVacancies'&& $idRout==null ) {

    $pattern = "/^[-,0-9a-zа-яё\s]+$/ui";

    if (isset($_POST['query']) && !empty($_POST['query']) && preg_match($pattern, $_POST['query'])) {
        $query = $_POST['query'];
        $title = $query;
        $date = date('Y-m-d');
        $siteLink = 'https://www.work.ua/jobs-' . $query . '/';

        $listVacancies = parseVacancyLink($siteLink);

        if ($listVacancies) {
            insertExplorer($db, $siteLink, $date, $title);
            $explorer_id= getSelectExplorerId($db, $siteLink);

            $id=$explorer_id[0]['id'];

            foreach ($listVacancies as $vacancy) {
                $insertVacancy = insertVacancy($db, $id, $vacancy);
            }

           $statusUpdate = updateStatusExplorer($db, $id);
            
            $vacancieslist = getSelectVacancy($db, $id);
            /*$vacancieslist = sql($db,'SELECT *, COUNT(`vacancy_title`) FROM `vacancy`
            WHERE explorer_id = '.$id.' GROUP BY create_date',[], 'rows');*/

            view('listVacancies', $data = ['listVacancies' => $vacancieslist]);

        }
        else {

            view('error');
        }
    }

            else {

                view('error');
            }

}