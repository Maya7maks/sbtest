<?php
$routs=[
    'listVacancies',
    'getVacancyDescription',
    'convertVacancy',
    'getNewVacancies',
    'listLinks',
    'wordProcess',
];

$action = null;
$subAction = null;
$idRout = null;

$serverName='188.166.95.167';
$serverUri='/';

if (array_key_exists('SERVER_NAME', $_SERVER) ) {
$serverName=$_SERVER['SERVER_NAME'];
$serverUri=$_SERVER['REQUEST_URI'];
}
if( $serverUri != '/' ) {
    $url =  parse_url($serverName.$serverUri);
    $urlArray = explode('/',$url['path']);

    $urlArray = array_filter($urlArray);
    $action = $urlArray[1];

    if( isset($urlArray[2]) ) {
        if(is_numeric($urlArray[2])){
            $idRout = $urlArray[2];
        }
        else $subAction = $urlArray[2];
    }
    if( !in_array( $action, $routs ) ) {
        $action = null;
        $subAction = null;
    }

}

else {
    $action = 'main';
}


