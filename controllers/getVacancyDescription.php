<?php
if( $action == 'getVacancyDescription' && $idRout ) {
    $tableName = 'vacancy_description';
    $id = $idRout;
    $col='vacancy_id';
    $description = getSelectAll($db, $tableName, $col, $id);
    $descriptDate = getCreatDate($db,'vacancy', 'id', $id);

    if (!empty($description)) {
        $descr=$description[0]['description'];
         view('descriptOfVacancy', $data=['description'=>$descr, 'id'=>$idRout, 'create_date'=>$descriptDate[0]['create_date']]);
    } else {
        $link = getLink($db, $idRout);
        if(isset($link)){
        $strDiscription = getDescript($link[0]);

        if (isset($strDiscription)) {

            insertVacancyDescript($db, $idRout, $strDiscription,$descriptDate[0]['create_date']);
           $vacancyUpdate = saveStatus($db, 'vacancy', $idRout, 'vacancy_status');
            view('descriptOfVacancy',  $data=['description'=>$strDiscription, 'id'=>$idRout, 'create_date'=>$descriptDate[0]['create_date']]);
        }

    }
    else{
        view('error');
        }
    }
}

